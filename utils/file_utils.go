package utils

import (
	"os"
	"fmt"
	"io/ioutil"
	"path/filepath"
)

func GetFilePathByExtension(args []string, fileExtension string) (string) {
	var filePath string

	// Use path from args if available
	// Or find file by extension
	if len(args) > 0 {
		filePath = args[0]
	} else {
		pwd, err := os.Getwd()
		if err != nil {
			fmt.Printf("error: %v", err)
			os.Exit(1)
		}

		var files []os.FileInfo
		files, err = ioutil.ReadDir(pwd)
		if err != nil {
			fmt.Printf("error: %v", err)
			os.Exit(1)
		}

		for _, dirFile := range files {
			if filepath.Ext(dirFile.Name()) == fileExtension {
				filePath = filepath.Join(pwd, dirFile.Name())
				break
			}
		}

		if filePath == "" {
			fmt.Printf("Could not find any %s file in the current directory (%s)", fileExtension, pwd)
			os.Exit(1)
		}
	}

	return filePath
}