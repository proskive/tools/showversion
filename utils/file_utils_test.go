package utils

import (
	"testing"
	"os"
	"path/filepath"
	"io/ioutil"
)

func TestGetFilePathByExtensionViaArgs(t *testing.T) {
	preparedFilePath := "/temp/testfile.xml"
	args := []string{ preparedFilePath }
	
	// Act
	foundFilePath := GetFilePathByExtension(args, ".xml")

	// Check
	if foundFilePath != preparedFilePath {
		t.Errorf("Wrong filepath was found. Expected: %s. Found: %s", preparedFilePath, foundFilePath)
	}
}

func TestGetFilePathByExtensionViaDiscovery(t *testing.T) {
	// Prepare file
	cwd, err := os.Getwd()
	if err != nil {
		t.Errorf( "error: %v", err)
		t.Fail()
	}

	testFileName := "testfile.xml"
	createdFilePath := filepath.Join(cwd, testFileName)
	err = ioutil.WriteFile(createdFilePath, []byte("test"), 777)
	if err != nil {
		t.Errorf( "error: %v", err)
	}

	// Act
	foundFilePath := GetFilePathByExtension([]string{}, ".xml")

	// Check
	if foundFilePath != createdFilePath {
		os.Remove(createdFilePath)
		t.Errorf("Wrong filepath was found. Expected: %s. Found: %s", createdFilePath, foundFilePath)
	}

	// Clean
	os.Remove(createdFilePath)
}