package cmd

import "github.com/spf13/cobra"

var RootCmd = &cobra.Command{
	Use:   "showversion",
	Short: "Short desc",
	Long:  "A helper tool for showing a version of a project.",
	Run: func(cmd *cobra.Command, args []string) {
		// Do Stuff
	},
}
