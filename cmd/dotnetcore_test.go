package cmd

import "testing"

func TestGetVersionFromCsprojWithVersionOnly(t *testing.T) {
	// Arrange
	csproj := `<Project Sdk="Microsoft.NET.Sdk.Web">
  <PropertyGroup>
    <Version>1.5.0</Version>
  </PropertyGroup>
</Project>`

	// Act
	foundVersion := getVersionFromCsproj([]byte(csproj))

	// Check
	if foundVersion != "1.5.0" {
		t.Errorf("Found wrong version. Expected: %s. Found: %s", "1.5.0", foundVersion)
	}
}

func TestGetVersionFromCsprojWithPrefixAndSuffix(t *testing.T) {
	csproj := `<Project Sdk="Microsoft.NET.Sdk.Web">
  <PropertyGroup>
    <VersionPrefix>2.4.1</VersionPrefix>
    <VersionSuffix>preview1</VersionSuffix>
  </PropertyGroup>
</Project>`

	// Act
	foundVersion := getVersionFromCsproj([]byte(csproj))

	// Check
	if foundVersion != "2.4.1-preview1" {
		t.Errorf("Found wrong version. Expected: %s. Found: %s", "2.4.1-preview1", foundVersion)
	}
}

func TestGetVersionFromCsprojWithoutVersion(t *testing.T) {
	csproj := `<Project Sdk="Microsoft.NET.Sdk.Web">
  <PropertyGroup>
  </PropertyGroup>
</Project>`

	// Act
	foundVersion := getVersionFromCsproj([]byte(csproj))

	// Check
	if foundVersion != "noversion" {
		t.Errorf("Found wrong version. Expected: %s. Found: %s", "noversion", foundVersion)
	}
}

func TestGetVersionFromCsprojWithPrefixOnly(t *testing.T) {
	csproj := `<Project Sdk="Microsoft.NET.Sdk.Web">
  <PropertyGroup>
	<VersionPrefix>2.0.0</VersionPrefix>
  </PropertyGroup>
</Project>`

	// Act
	foundVersion := getVersionFromCsproj([]byte(csproj))

	// Check
	if foundVersion != "2.0.0" {
		t.Errorf("Found wrong version. Expected: %s. Found: %s", "2.0.0", foundVersion)
	}
}

func TestGetVersionFromCsprojWithSuffixOnly(t *testing.T) {
	csproj := `<Project Sdk="Microsoft.NET.Sdk.Web">
  <PropertyGroup>
	<VersionSuffix>beta</VersionSuffix>
  </PropertyGroup>
</Project>`

	// Act
	foundVersion := getVersionFromCsproj([]byte(csproj))

	// Check
	if foundVersion != "beta" {
		t.Errorf("Found wrong version. Expected: %s. Found: %s", "beta", foundVersion)
	}
}