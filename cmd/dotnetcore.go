package cmd

import (
	"github.com/spf13/cobra"
	"encoding/xml"
	"fmt"
	"io/ioutil"
	"os"
	"gitlab.com/proskive/tools/showversion/utils"
)

var dotnetCoreCmd = &cobra.Command{
	Use: "dotnetcore",
	Short: "Shows version of a .NET Core project",
	Long: "Shows version of a .NET Core project within the folder or specify a .csproj file.",
	Run: dotnetCoreRun,
}

func init() {
	RootCmd.AddCommand(dotnetCoreCmd)
}

type DotnetCoreResult struct {
	VersionPrefix string `xml:"PropertyGroup>VersionPrefix"`
	Version string `xml:"PropertyGroup>Version"`
	VersionSuffix string `xml:"PropertyGroup>VersionSuffix"`
}

func dotnetCoreRun(cmd *cobra.Command, args []string) {
	// Find file
	file := utils.GetFilePathByExtension(args, ".csproj")

	// Read file
	xmlBytes, err := ioutil.ReadFile(file)
	if err != nil {
		fmt.Printf("error: %v", err)
		os.Exit(1)
	}

	// Print version
	fmt.Println(getVersionFromCsproj(xmlBytes))
}

func getVersionFromCsproj(fileContent []byte) (string) {
	var v DotnetCoreResult
	err := xml.Unmarshal([]byte(fileContent), &v)
	if err != nil {
		fmt.Printf("error: %v", err)
		os.Exit(2)
	}

	// "noversion"
	if v.Version == "" && v.VersionPrefix == "" && v.VersionSuffix == "" {
		return "noversion"
	}

	// "x.y.z"
	if v.Version != "" {
		return fmt.Sprintf("%s", v.Version)
	}

	if v.VersionPrefix != "" && v.VersionSuffix == "" {
		return fmt.Sprintf("%s", v.VersionPrefix)
	}

	// "abc"
	if v.VersionPrefix == "" && v.VersionSuffix != "" {
		return fmt.Sprintf("%s", v.VersionSuffix)
	}

	// "x.y.z-abc"
	return fmt.Sprintf("%s-%s", v.VersionPrefix, v.VersionSuffix)
}
