package main

import (
	"gitlab.com/proskive/tools/showversion/cmd"
)

func main() {
	cmd.RootCmd.Execute()
}
